/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo01;

/**
 *
 * @author rodri
 */
public class Persona {
    
    //Seccion de atributos
    private int id;
    private String nombre;
    private String aPaterno;
    private String aMaterno;
    private String direccion;
    
    //Estado del objeto = "que valores tienen los atributos del objeto"

    //Constructor
    public Persona() {
        
    }

    //Constructor sobrecargado
    public Persona(int id, String nombre, String aPaterno, String aMaterno, String direccion) {
        this.id = id;
        this.nombre = nombre;
        this.aPaterno = aPaterno;
        this.aMaterno = aMaterno;
        this.direccion = direccion;
    }
    
    //Metodos de acceso
    public int getId() {
        return id;
    }

    public void setId(int id) throws Exception{
        if(id >= 0){
            this.id = id;
        }else{
            throw new Exception("ID no debe ser negativo");
        }
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) throws Exception{
        if(!nombre .isEmpty()){
            this.nombre = nombre;
        }else{
            throw new Exception("Nombre no debe estar vacío");
        }
    }

    public String getaPaterno() {
        return aPaterno;
    }

    public void setaPaterno(String aPaterno) {
        this.aPaterno = aPaterno;
    }

    public String getaMaterno() {
        return aMaterno;
    }

    public void setaMaterno(String aMaterno) {
        this.aMaterno = aMaterno;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
}
