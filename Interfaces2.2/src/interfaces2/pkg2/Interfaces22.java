/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces2.pkg2;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author rodri
 */
public class Interfaces22 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Declaracion de la clase Scaner para optener datos del teclado del usuario
        Scanner s = new Scanner(System.in);
   
        //Declaracion de arreglos a sus respectivas areas
        float administrativo[] = new float[5];
        float ventas[] = new float[2];
        float operativo[] = new float[8];
       
        //Try Catch para evitar que instroduscan caracteres espesiales
        try {
            //for donde preguntaremos las horas de cada una de las 5 personas adminstrativas
        for (int i = 0; i < administrativo.length; i++) {
            System.out.println("Introduce las horas de la persona " + (i+1) + " de administrativo");
            int val = s.nextInt();
            Nomina a = new Administrativo(val);
            administrativo[i] = a.calcularNomina();
        }
        
        //for donde preguntaremos las ventas de cada una de las 2 personas de ventas
        for (int i = 0; i < ventas.length; i++) {
            System.out.println("Introduce las ventas de la persona " + (i+1));
            int val = s.nextInt();
            Nomina v = new Ventas(val);
            ventas[i] = v.calcularNomina();
        }
        
        //for donde preguntaremos las horas de cada una de las 5 personas operativas
        //preguntaremos tambien si es que tienen horas extra y realizar su respetivo analisis
        for (int i = 0; i < operativo.length; i++) {
            System.out.println("Introduce las horas de la persona " + (i+1) + " de operativo");
            int val = s.nextInt();
            System.out.println("Introduce las horas extra de la persona " + (i+1) + " de operativo");
            int hrs = s.nextInt();
            Nomina o = new Operativo(val, hrs);
            operativo[i] = o.calcularNomina();
        }
        } catch (Exception e) {
            System.out.println("Solo se permite intriducir combinaciones de numeros del 0 al 9");
        }
        
        System.out.println("Calculando nomina...");
        
        new Timer().schedule(new TimerTask() {
        @Override
        public void run() {
            for (int i = 0; i < 30; i++) {
                System.out.println("");
            }
            calcular(administrativo, ventas, operativo);
        }}, 5000);
        
    }

    //Metodo donde calculamos e imprimimos el valor de la nomina
    private static void calcular(float[] administrativo, float[] ventas, float[] operativo) {
        float totalNomina = 0;
        for (int i = 0; i < administrativo.length; i++) {
            totalNomina += administrativo[i]; 
        }
        
        for (int i = 0; i < ventas.length; i++) {
            totalNomina += ventas[i];
        }
        
        for (int i = 0; i < operativo.length; i++) {
            totalNomina += operativo[i];
        }
        
        System.out.println("La nomina total es de $" + totalNomina);
    }
    
}
