/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces2.pkg2;

/**
 *
 * @author rodri
 */
//Implementacion de la interface Nomina a la clase Administrativo
public class Administrativo implements Nomina{

    //Variable ha utilizar
    private int numHoras;

    //Sobreargando el constructor con su respectiva variable
    public Administrativo(int numHoras) {
        this.numHoras = numHoras;
    }

    //Get y Set de la variable
    public int getNumHoras() {
        return numHoras;
    }

    public void setNumHoras(int numHoras) {
        this.numHoras = numHoras;
    }

    //Metodo abstracto de la interfaz Nomina
    @Override
    public float calcularNomina() {
       return 42 * numHoras * 40;
    }
  
}
