package Banco;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Principal extends javax.swing.JFrame {

    public Principal() {
        initComponents();
        //Inavilitamos los editsText
        txt_obsequio.setEnabled(false);
        txt_cuenta.setEnabled(false);
        txt_taza_interes.setEnabled(false);
        txt_salida.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txt_nombre = new javax.swing.JTextField();
        txt_direccion = new javax.swing.JTextField();
        txt_edad = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        targeta = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txt_cuenta = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_valor = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_obsequio = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txt_salida = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_taza_interes = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        btn_calcular = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Tu nombre:");

        jLabel2.setText("Tu dirección:");

        txt_nombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nombreActionPerformed(evt);
            }
        });

        jLabel3.setText("Tu edad:");

        targeta.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No Tengo", "Nominal", "Pension" }));

        jLabel4.setText("Tarjeta Bancaria");

        jLabel6.setText("Cuenta:");

        jLabel7.setText("Ingresa un valor:");

        jLabel8.setText("Obsequio:");

        jLabel9.setText("Salida:");

        jLabel10.setText("Taza de interes:");

        btn_calcular.setText("Calcular");
        btn_calcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_calcularActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel3)
                                .addComponent(jLabel4))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(456, 456, 456)
                                    .addComponent(btn_calcular))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(361, 361, 361)
                                    .addComponent(jLabel7))))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(10, 10, 10)
                                            .addComponent(jLabel8))
                                        .addComponent(txt_obsequio, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(targeta, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel6)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txt_cuenta)
                                            .addComponent(txt_salida)
                                            .addComponent(txt_taza_interes, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                                            .addComponent(jLabel10))
                                        .addComponent(jLabel9))))
                            .addGap(37, 37, 37))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(txt_edad, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_valor, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(txt_direccion, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_direccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_valor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_edad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(btn_calcular))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(jLabel6))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(targeta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_cuenta, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_obsequio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_taza_interes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(14, 14, 14)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_salida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

  private void txt_nombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nombreActionPerformed
//No supimos como borrarlo xD
  }//GEN-LAST:event_txt_nombreActionPerformed

    //Codificacion de la logica de la aplicacion
  private void btn_calcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_calcularActionPerformed
      //Declaracion de clase de el Jpanel para los JOptionPanel
      final JPanel panel = new JPanel();

      //Declaracion, iniclaizacion de algunas variables
      int edad = 0;
      String nombre = txt_nombre.getText();
      String tarjeta = targeta.getSelectedItem().toString();
      double valor = 0;

      //Condicion para la edad que no sea 0 y que no sean letras
      try {
          edad = Integer.parseInt(txt_edad.getText());
          if (edad <= 0) {
              JOptionPane.showMessageDialog(panel, "La edad debe de ser un número", "Warning", JOptionPane.WARNING_MESSAGE);
              return;
          }
      } catch (Exception e) {
          JOptionPane.showMessageDialog(panel, "La edad debe de ser un valor númerico", "Warning", JOptionPane.WARNING_MESSAGE);
      }

      //Condicion para ver si el valor no son letras
      try {
          valor = Double.parseDouble(txt_valor.getText());
      } catch (Exception e) {
          JOptionPane.showMessageDialog(panel, "El valor debe de ser un número", "Warning", JOptionPane.WARNING_MESSAGE);
      }

      //If que dice si el valor es mayor a 0
      if (valor > 0) {
          //Factory f = new Factory();
          
          //Cuenta10 c10 = new Factory().extender(edad, valor, tarjeta);
          Cuenta c = new Factory().extender(edad, valor, nombre);
          
          JOptionPane.showMessageDialog(panel, c.cotizar(valor), "Warning", JOptionPane.WARNING_MESSAGE);
          
          //System.out.println(c.cotizar(valor));
      } else {
          JOptionPane.showMessageDialog(panel, "Introduzca un precio mayor a 0", "Warning", JOptionPane.WARNING_MESSAGE);
      }

  }//GEN-LAST:event_btn_calcularActionPerformed

    /*private void banco(int edad, double valor, String valor2) {
        //If que decidira que es cuenta joven
        if (edad >= 18 && edad <= 25 && valor2.equals("NINGUNA")) {

            Cuenta c = new CuentaJoven();
            double total = c.cotizar(valor);
            CuentaJoven c1 = new CuentaJoven();
            double interes = c1.getInteres();
            txt_taza_interes.setText(interes + "%");
            txt_obsequio.setText(c1.getRegalo());
            txt_salida.setText(total + "");
            txt_cuenta.setText("Cuenta Joven");

            //If que decidira si es cuenta 10
        } else if (edad > 25 && edad < 65 && valor2.equals("NOMINA")) {

            Cuenta c = new Cuenta10();
            double total = c.cotizar(valor);
            Cuenta10 c1 = new Cuenta10();
            double interes = c1.getInteres();
            txt_salida.setText(total + "");
            txt_cuenta.setText("Cuenta 10");
            txt_taza_interes.setText(interes + "%");
            txt_obsequio.setText(c1.getRegalo());

            //If que decidira que es cuenta Oro
        } else if (edad >= 65 && valor2.equals("PENSION")) {

            Cuenta c = new CuentaOro();
            double total = c.cotizar(valor);
            CuentaOro c1 = new CuentaOro();
            double interes = c1.getInteres();
            txt_salida.setText(total + "");
            txt_cuenta.setText("Cuenta Oro");
            txt_taza_interes.setText(interes + "%");
            txt_obsequio.setText(c1.getRegalo());

            //Termino de If que decidira que es cuenta estandar
        } else {

            Cuenta c = new CuentaEstandar();
            double total = c.cotizar(valor);
            CuentaEstandar c1 = new CuentaEstandar();
            double interes = c1.getInteres();
            txt_salida.setText(total + "");
            txt_cuenta.setText("Cuenta estandar");
            txt_taza_interes.setText(interes + "%");
            txt_obsequio.setText(c1.getRegalo());
        }
    }*/

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_calcular;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JComboBox<String> targeta;
    private javax.swing.JTextField txt_cuenta;
    private javax.swing.JTextField txt_direccion;
    private javax.swing.JTextField txt_edad;
    private javax.swing.JTextField txt_nombre;
    private javax.swing.JTextField txt_obsequio;
    private javax.swing.JTextField txt_salida;
    private javax.swing.JTextField txt_taza_interes;
    private javax.swing.JTextField txt_valor;
    // End of variables declaration//GEN-END:variables
}
