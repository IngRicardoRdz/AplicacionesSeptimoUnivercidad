/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversiones;

import java.util.Scanner;

/**
 *
 * @author rodri
 */
public class Longitud {
    
    //Se inicializa la instancia para poder meter datos por medio del teclado de manera global
    Scanner s = new Scanner (System.in) ;  

    public Longitud() {
        menu();
    }        
    
    //Metodo donde realizamos la tarea de convertir metros a yardas
    private void MetrosYarda(){
        System.out.println("Ingrese metros a convertir:");
        double Metro = s.nextDouble();
        
        double resultado = Metro *1.0936;
        System.out.println("El resultdo es:" + resultado+" yardas");              
    }
    
    //Metodo donde realizamos la tarea de convertir yardas a metros
    private void YardaMetros(){
        System.out.println("Ingrese yardas a covertir:");
        double Yardas = s.nextDouble();
        
        double resultado = Yardas / 1.0936;
        System.out.println("El resultado es:" + resultado+" metros");
    }

    //Menu donde se le preguntara al usuario que conversion decea realizar
    private void menu() {
        System.out.println("¿Que desea hacer?");
        System.out.println("*****************");
        System.out.println("1. Metros a Yardas ");
        System.out.println("2. Yardas a Metros");
        System.out.print("Seleccione la opción deseada: "); int seleccion = s.nextInt();
        switch(seleccion){
            case 1:
               MetrosYarda();
                break;
            case 2:
              YardaMetros();
                break;
            default:
                System.out.println("La opcion es incorrecta");
                menu();
                break;
        }
    }
}
