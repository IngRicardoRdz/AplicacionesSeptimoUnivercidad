/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversiones;

import java.util.Scanner;

/**
 *
 * @author rodri
 */
public class Moneda {
   
    //Se inicializa la instancia para poder meter datos por medio del teclado de manera global
  Scanner s = new Scanner(System.in);
  
  //Constructor que manda a llamar al metodo menu
 public Moneda(){
     
   
     menu();
    
}
 
 //Metodo donde se realiza la conversion de peso a dolar
 private void monedaDolar(){
      System.out.print("Ingrese los pesos: ");
        double Dolar = s.nextDouble();
     
          double res =Dolar/18.90;
        System.out.println("El resultado es: "+res+" dolares");
    
}
 
  //Metodo donde se realiza la conversion de dolar a peso
 private void dolarMoneda(){
  
         System.out.print("Ingrese los dolares: ");
        double Moneda = s.nextDouble();
     
          double res =Moneda*18.90;
        System.out.println("El resultado es: "+res+" pesos");
     
    
}

 //Menu donde se le preguntara al usuario que conversion decea realizar
    private void menu() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
        System.out.println("¿Que desea hacer?");
        System.out.println("*****************");
        System.out.println("1. Dolar a Moneda ");
        System.out.println("2. Moneda a Dolar");
        System.out.print("Seleccione la opción deseada: "); int seleccion = s.nextInt();
        switch(seleccion){
            case 1:
                dolarMoneda();
                break;
            case 2:
                monedaDolar();
                break;
            default:
                System.out.println("La opcion es incorrecta");
                menu();
                break;
    
    }
             
}
}