/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojos;

/**
 *
 * @author rodri
 */
public class CuentaDiez {
    private static double tasaInteres = 1;
    private static int tDebito = 0;
    private static int tCredito = 18;
    private static int mtCredito = 60;
    
    private static int descuento = 50;
    private static String regalo = "Reproductor CD";

    public static double getTasaInteres() {
        return tasaInteres;
    }

    public static int gettDebito() {
        return tDebito;
    }

    public static int gettCredito() {
        return tCredito;
    }

    public static int getMtCredito() {
        return mtCredito;
    }

    public static int getDescuento() {
        return descuento;
    }

    public static String getRegalo() {
        return regalo;
    }
    
    
}
