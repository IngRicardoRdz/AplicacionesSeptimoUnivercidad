package modelo;

public class ModeloProveedores {
    
    //Declaracion de variables que utilizaran los Proveedores
    private String nombre;
    private String direccion;
    private String telefono;

    public ModeloProveedores() {
    }
    
    //Constructor que iniciaiza las variables de clase
    public ModeloProveedores(int id, String nombre, String direccion, String telefono) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
}
