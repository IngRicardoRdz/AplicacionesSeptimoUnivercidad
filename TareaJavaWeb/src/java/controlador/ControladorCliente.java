package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ClientesDAO;
import modelo.ModeloCliente;
import singleton.Singleton;

@WebServlet(name = "cliente.template", urlPatterns = {"/cliente.template"})
public class ControladorCliente extends HttpServlet {

    private ModeloCliente modelo;
    private PrintWriter out;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        out = resp.getWriter();

        try {
            modelo = new ModeloCliente();
            //Inicialisacion de la clase ClientesDAO con la instancia del patron de diseño Singleton
            ClientesDAO p = new ClientesDAO(Singleton.crearConexion());
            //Asignacion al modelo Cliente de cada uno de los datos traidos de la vista cliente 
            modelo.setNombre(req.getParameter("nombres"));
            modelo.setApellido(req.getParameter("apellidos"));
            modelo.setDireccion(req.getParameter("direccion"));
            modelo.setTelefono(req.getParameter("telefono"));
            //Mandando la clase modelo para la clase ClientesDAO para realizar el insert de los datos en la base de datos
            p.add(modelo);
            out.print("<script>alert('Datos Alacenados');</script>");
            resp.sendRedirect("index.jsp");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error Class: " + ex);
        } catch (SQLException ex) {
            System.out.println("Error SQL: " + ex);
        }

    }

}
